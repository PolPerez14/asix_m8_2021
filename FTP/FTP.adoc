
= Pràctica FTP

'''
== [underline]#Apartat 1: Instal·lador del servidor FTP a Fry#
image::Screenshot_1.png[]
*Exemple 1:* Captura transacció FTP entre client i FRY

'''
== [underline]#Apartat 2: Configuració del accés anònim#
image::Screenshot_2.png[]
*Exemple 2:* Captura de la sortida ls-ld sobre el nou directori

'''
image::Screenshot_3.png[]
*Exemple 3:* Captura de les modificacions de la configuració

'''
image::Screenshot_4.png[]
*Exemple 4:* Captura d' una transacció utilitzant l' accés anònim

== [underline]#Apartat 3: Separació de la configuració del FTP en xarxes#
image::Screenshot_5.png[]
image::Screenshot_6.png[]
*Exemple 5.1:* Canvis al fitxer de configuració

'''
image::Screenshot_7.png[]
*Exemple 6:* Fitxer /etc/hosts.allow

'''
image::Screenshot_8.png[]
*Exemple 7:* Comprovació accés exterior

'''
image::Screenshot_9.png[]
*Exemple 8:* Comprovació accés interior

== [underline]#Apartat 4: Configuració espai compartit#

image::Screenshot_10.png[]
*Exemple 9:* Ordre utilitzada per crear l' usuari ftpuser

'''
image::Screenshot_11.png[]
*Exemple 10:* Ordre utilitzada per donar una contrasenya a l' usuari ftpuser

'''
image::Screenshot_12.png[]
*Exemple 11:* Comprovació que l' usuari ftpuser pot connectar-se al servei FTP

'''
image::Screenshot_13.png[]
*Exemple 12:* Ordre per reiniciar el servidor FTP

'''
image::Screenshot_14.png[]
*Exemple 13:* Instrucció per treure el permís d’escriptura al home de l’usuari ftpuser.

'''
image::Screenshot_15.png[]
image::Screenshot_16.png[]
*Exemple 14:* Comprovació que ftpuser i anonymous poden connectar, però que el teu usuari
habitual no.

== [underline]#Apartat 5: Configuració de l' espai per a cada usuari#

image::Screenshot_17.png[]
image::Screenshot_18.png[]
*Exemple 15:* Canvis fets a /etc/vsftpd.lan.conf.













